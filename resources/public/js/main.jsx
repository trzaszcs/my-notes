import React from 'react';
import ReactDOM from 'react-dom';
import api from './api';
import disp from './event/dispatcher';
import MainContainer from './component/MainContainer.jsx';
import Dashboard from './component/Dashboard.jsx';
import App from './component/App.jsx';
import About from './component/About.jsx';
import SetDetails from './component/SetDetails.jsx';
import SetList from './component/SetList.jsx';
import { Router, Route, IndexRoute, hashHistory } from 'react-router'


ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Dashboard} />
      <Route path="about" component={About}/>
      <Route path="sets" component={SetList}/>
      <Route path="setId=*" component={SetDetails}/>
    </Route>
  </Router>
, document.getElementById('container'));

disp.emit({type:'VIEW_CHANGED', view:'SETS_LIST'});
api.getSets((sets)=>{
  disp.emit({type:'SETS_RETURNED', sets: sets});
});
