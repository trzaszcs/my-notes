
var Dispatcher = (function (){
  var listeners = {};

  function addListener(eventType, listener){
    listeners[eventType] = listener;
  }

  function dispatch(event){
    console.log("dispatching event:", event);
    let listener = listeners[event.type];
    if(listener){
      listener(event);
    }
  }

  return {
    registerListener: addListener,
    emit: dispatch
  };
})();

export default Dispatcher;
