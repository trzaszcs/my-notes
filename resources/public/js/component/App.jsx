import React from 'react';
import { Link } from 'react-router'

class App extends React.Component {

  constructor(props){
    super(props);
  }

  onClick(e){
    console.log("--->",e);
  }

  render() {
    return (
      <div>
        <h1>App</h1>
        <ul>
          <li><Link to="about" onClick={this.onClick}>About</Link></li>
          <li><Link to="sets" onClick={this.onClick}>Sets</Link></li>
          <li><Link to="setId=1" onClick={this.onClick}>Set Details = 1</Link></li>
        </ul>
        {this.props.children}
      </div>
    );
  }
}

export default App;
