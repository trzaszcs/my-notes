import React from 'react';
import ReactDOM from 'react-dom';
import api from './../api'

class SetList extends React.Component {

  constructor(props){
    super(props);
    this.state = {sets:[]};
    api.getSets((sets)=>this.setState({sets: sets}));
  }

  onClick(){
  	console.log("clicked");
  	window.location.hash = 'add-set';
  }

  onSetSelect(id){
  	window.location.hash = 'setId='+id;
  }

  render() {
    return (
      <div>
        <h2 className="ui center aligned header">Sets</h2>
    	  <button className="ui button" onClick={this.onClick}>Add Set</button>
        <div className="ui segments">
        			{this.state.sets.map((set) => {
        				return (<div className="ui segment" onClick={()=>{this.onSetSelect(set.id)}} key={set.id}>{set.name}</div>);
        			})}
        </div>
      </div>
    );
  }
}

export default SetList;
