import React from 'react';
import ReactDOM from 'react-dom';
import SetList from './SetList.jsx';
import SetForm from './SetForm.jsx';

class MainContainer extends React.Component {

  constructor(props){
    super(props);
    this.state = {view: ''};
    props.dispatcher.registerListener('VIEW_CHANGED',(event) => {
      this.setState({view: event.view});
    });
  }

  render() {
    let view;
    switch(this.state.view) {
      case 'SETS_LIST':
        view = <SetList dispatcher={this.props.dispatcher}/>;
        break;
      case 'ADD_SET':
        view = <SetForm dispatcher={this.props.dispatcher}/>;
        break;
      case 'EDIT_LIST':
        break;
      default:
        break;
    }
    return (<div className="ui container">{view}</div>);
  }
}

export default MainContainer;
