import React from 'react';
import ReactDOM from 'react-dom';

class SetForm extends React.Component {

  constructor(props){
    super(props);
    this.state = {name: '', description: ''};
  }

  handleNameChange(e){
    this.setState({name: e.target.value});
  }

  handleDescriptionChange(e){
    this.setState({description: e.target.value});
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.dispatcher.emit({type: '', data: ''})
  }

  render() {
    return (
    <form onSubmit={this.handleSubmit}>
      <input
        type="text"
        placeholder="Name..."
        value={this.state.name}
        onChange={this.handleNameChange}/>
      <input
        type="text"
        placeholder="Description..."
        value={this.state.description}
        onChange={this.handleDescriptionChange}
      />
      <input type="submit" value="Post" />
    </form>
    );
  }
}

export default SetForm;
