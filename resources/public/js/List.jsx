import React from 'react';
import ReactDOM from 'react-dom';

class SetList extends React.Component {
  onClick(){
  	console.log("clicked");
  	window.location.hash = 'add-set';
  }
  
  render() {
	var sets = this.props.sets;
    return (<div className="ui segments">
    			{sets.map(function(set){
    				return (<div className="ui segment" key={set.id}>{set.name}</div>);
    			})}
    	   </div>);
  }
}