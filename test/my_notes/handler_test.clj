(ns my-notes.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [my-notes.handler :refer :all]))

(deftest test-app
  (testing "main route"
    (let [response (app (mock/request :get "/"))]
      (is (= (:status response) 200))
      (is (= (:body response) "Hello World"))))
  
  (testing "sets route"
    (let [response (app (mock/content-type (mock/request :get "/sets") "application/json"))]
      (is (= (:status response) 200))))

  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 404)))))
