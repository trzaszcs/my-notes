var path = require('path');

module.exports = {
        entry: './resources/public/js/main.js',
        output: {
            path: './resources/public/dist',
            filename: 'bundle.js'
        },
        module: {
            loaders: [
                { 
                	test: /\.jsx?$/, 
                	exclude: /node_modules/,
                	loader: 'babel-loader' ,
                	query: {
                        presets: ['es2015', 'react']
                    }
                }
            ]
        },
        externals: {
            "jquery": "jQuery"
        }
    };