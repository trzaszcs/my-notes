(ns my-notes.sets
  (:require [ring.util.response :refer [response]]
            [my-notes.dao :as dao]))

(defn get-sets 
  []
  (response (dao/get-sets)))

(defn get-set
 [id]
 (response (dao/get-set id)))

(defn add
 [request]
 (println "->" request)
 (response {:id (dao/save-set (:body request))}))