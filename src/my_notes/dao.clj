(ns my-notes.dao)

(def db-set (atom [{:id "f540214b-0bdd-4993-b19b-8d2500e3b967":name "set1"} {:id "f6aa5eae-d81d-4b2a-b65f-036ecf11b001":name "set2"}]))

(defn gen-uid
  []
  (str (java.util.UUID/randomUUID)))

(defn save-set
  [data]
  (let [id (gen-uid)]
   (swap! db-set (fn [set] (assoc set id data)))))

(defn get-set
  [id]
  (first (filter #(= (% :id) id) @db-set)))

(defn get-sets
  []
  @db-set)
