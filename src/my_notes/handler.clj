(ns my-notes.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [my-notes.sets :as sets]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [ring.util.response :refer [response resource-response redirect]]))

(defroutes app-routes
  (GET "/" [] (redirect "/index.html")) 
  (GET "/sets" [] (sets/get-sets))
  (GET "/sets/:id" [id] (sets/get-set id))
  (POST "/sets" [] sets/add)
;  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (-> app-routes
    wrap-json-response
    wrap-json-body
    (wrap-defaults (assoc-in site-defaults [:security :anti-forgery] false))))
